var $menu = jQuery('.searchIcon, .searchModule');

jQuery(document).mouseup(function (e) {
if (!$menu.is(e.target) && $menu.has(e.target).length === 0 && jQuery('.searchModule').css('display') == 'block') {
jQuery('.searchModule').slideToggle();
}
});

jQuery('.searchIcon').click(function(){
    
    jQuery('.searchModule').slideToggle();
    
})  

jQuery(window).scroll(function(){
    if (jQuery(this).scrollTop() > 164) {
        jQuery('.fixed-header').addClass('scroll-none');
        jQuery('.sticky-header').addClass('sticky-header-active');
    } else {
        jQuery('.fixed-header').removeClass('scroll-none');
        jQuery('.sticky-header').removeClass('sticky-header-active');
    }
});




